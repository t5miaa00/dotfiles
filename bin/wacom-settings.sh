#!/bin/bash

# Script optimized for my wacom intuos5
# Sets the hotkeys for the buttons and also sets the button
stylus=`xsetwacom --list devices | grep 'STYLUS' | cut -f2 | sed -e 's/id: //'`
eraser=`xsetwacom --list devices | grep 'ERASER' | cut -f2 | sed -e 's/id: //'`
pad=`xsetwacom --list devices | grep 'PAD' | cut -f2 | sed -e 's/id: //'`
touch=`xsetwacom --list devices | grep 'TOUCH' | cut -f2 | sed -e 's/id: //'`

# If none are found; Exit.
if [[ -z $stylus && -z $eraser && -z $touch ]]; then
    echo "No devices found. Exiting."
    exit
fi

# -- Parameters --
for i in "$stylus" "$eraser"; do
    xsetwacom --set $i rawsample 10
done
xsetwacom --set $pad gesture on
xsetwacom --set $pad touch on

# -- Keybinds --
# Set up for Krita in particular
# Top 4 buttons.
btn1="button 2 key +ctrl z -ctrl" #2 [ ]
btn2="button 3 key +ctrl +shift z -ctrl -shift" #3 [-]
btn3="Button 8 " #8 [*]
btn4="Button 9 " #9 [ ]

# Bottom 4 buttons.
btn5="Button 10 " #10 [ ]
btn6="Button 11 " #11 [*]
btn7="Button 12 " #12 [-]
btn8="Button 13 " #13 [ ]

# Wheel and deal.
whup1=""
whdwn1=""

whup2=""
whdwn2=""

whup3=""
whdwn3=""

whup4=""
whdwn4=""

# Setting up the keybinds
for i in "$btn1" "$btn2"; do
    xsetwacom --set $pad $i
done
