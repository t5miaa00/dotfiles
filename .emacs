(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(custom-enabled-themes (quote (misterioso)))
 '(display-battery-mode t)
 '(org-agenda-files (quote ("~/koulu/ruotsi.org")))
 '(package-selected-packages (quote (company tide all-the-icons neotree evil)))
 '(scroll-bar-mode nil)
 '(tool-bar-mode nil)
 '(tool-bar-position (quote right)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Hack" :foundry "simp" :slant normal :weight normal :height 90 :width normal)))))
;; Emacs terminal with transcluent bg
(defun on-after-init()
  (unless (display-graphic-p(selected-frame))
    (set-face-background 'default "unsecified-bg" (selected-frame))))

(add-hook 'window-setup-hook 'on-after-init)
;; Window switching
(windmove-default-keybindings)
(setq windmove-wrap-around t)

;; Tide typescript
(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  ;; company is an optional dependency. You have to
  ;; install it separately via package-install
  ;; `M-x package-install [ret] company`
  (company-mode +1))
;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)
;; formats the buffer before saving
(add-hook 'before-save-hook 'tide-format-before-save)
(add-hook 'typescript-mode-hook #'setup-tide-mode)

;; I C O N S
(require 'all-the-icons)

;; Neotree
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))

;; Tabbing and shit
;;(define-key read-expression-map [(tab)] 'hippie-expand)
;;(defun hippie-expand ()
;;  (interactive)
;;  (hippie-expand 0))

(define-key read-expression-map [(tab)] 'hippie-unexpand)

;;(set-frame-font "Hack 9" nil t)
;; Setting up evil-mode...
;;(require 'evil)
;;(evil-mode 1)
