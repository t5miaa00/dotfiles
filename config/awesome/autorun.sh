#!/usr/bin/env bash

export QT_QPA_PLATFORMTHEME=gtk2

# usefull run function
function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

# run commands:
run compton --config=/home/aatu/.config/compton.conf
