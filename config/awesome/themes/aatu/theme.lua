---------------------------
-- Default awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gears = require("gears")
local gfs = require("gears.filesystem")
local themes_path = gfs.get_xdg_config_home().."awesome/themes/aatu/"
local img_dir = "/home/aatu/Kuvat/"
local wallpaper_dir = img_dir.."/taustakuvat/"

local wallpapers = {
   "8777da0e95e26f10aa2cd7ad4c4fcf10.png",
   "e747cd02c13fba5a65bfe1111513a8c6.png"
}

local theme = {}

theme.font          = "mononoki 10"
theme.taglist_font  = "Ubuntu 10"

theme.bg_normal     = "#000000"
theme.bg_focus      = "#252525" -- 0x55 + 0x25 = 0x70 ??
theme.bg_focus_tb   = "#000000"
theme.transparent   = "#00000000"
theme.bg_urgent     = "#E25852"
theme.bg_minimize   = theme.transparent
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#dfdfdf"
theme.fg_focus      = "#50ccff"
theme.fg_urgent     = "#F5F6F7"
theme.fg_minimize   = "#dfdfdf70"

theme.useless_gap   = dpi(2)
theme.border_width  = 0
theme.border_normal = theme.bg_normal
theme.border_focus  = theme.bg_focus
theme.border_marked = theme.bg_urgent

theme.menu_height = 22  --18

theme.notification_width = "300"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
theme.titlebar_bg_focus = theme.bg_focus_tb
theme.titlebar_bg_normal = theme.bg_normal

theme.taglist_bg_focus = theme.bg_focus

theme.tasklist_bg_focus = theme.bg_focus
theme.tasklist_bg_normal = theme.transparent

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."icons/chevron-right_w.svg"
theme.menu_width  = dpi(150)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = themes_path.."icons/x_u_w.svg"
theme.titlebar_close_button_focus  = themes_path.."icons/x_w.svg"

theme.titlebar_maximized_button_normal_inactive = themes_path.."icons/maximize_u_w.svg"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."icons/maximize_w.svg"
theme.titlebar_maximized_button_normal_active = themes_path.."icons/maximize_u_w.svg"
theme.titlebar_maximized_button_focus_active  = themes_path.."icons/maximize_w.svg"

theme.titlebar_minimize_button_normal_inactive = themes_path.."icons/minimize_u_w.svg"
theme.titlebar_minimize_button_focus_inactive  = themes_path.."icons/minimize_w.svg"
theme.titlebar_minimize_button_normal_active = themes_path.."icons/minimize_u_w.svg"
theme.titlebar_minimize_button_focus_active  = themes_path.."icons/minimize_w.svg"

theme.wallpaper = function (s)
   return wallpaper_dir .. wallpapers[s.index]
end


-- You can use your own layout icons like this:
theme.layout_floating   = themes_path.."icons/layers_w.svg"
theme.layout_max        = themes_path.."icons/maximize_w.svg"
theme.layout_tile       = themes_path.."icons/grid_w.svg"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = "Papirus-Light"

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
