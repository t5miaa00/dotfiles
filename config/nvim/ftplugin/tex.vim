inoremap <buffer> <Leader>ns <esc>o\section{}<esc>i
inoremap <buffer> <Leader>nns <esc>o\subsection{}<esc>i
inoremap <buffer> <Leader>nnns <esc>o\subsubsection{}<esc>i
inoremap <buffer> <Leader>ncb <esc>o\begin{lstlisting}<return>\end{lstlisting}<esc>O
inoremap <buffer> <Leader>acs <esc>a\acrshort{}<esc>i
inoremap <buffer> <Leader>acf <esc>a\acrfull{}<esc>i

if !exists("commands_loaded")
	let commands_loaded = 1
	command LatexComp call LatexComp()
endif

function! LatexComp()
	execute ":silent !latexcomp %"
endfunction
