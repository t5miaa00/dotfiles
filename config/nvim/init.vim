"{{{ Plugged

call plug#begin(stdpath('data') . '/vim-plug')

Plug 'vimwiki/vimwiki'
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"Plug 'Shougo/deoplete-clangx'
Plug 'tikhomirov/vim-glsl'

call plug#end()

"}}}

" Not a vi
set nocompatible

" Deoplete settings
"let g:deoplete#enable_at_startup = 1
"call deoplete#custom#var('clangx', 'clang_binary', '/usr/bin/clang')
"call deoplete#custom#var('clangx', 'default_c_options', '')

" Clipboard
set clipboard+=unnamedplus

" Completions
set complete+=i

"set termguicolors

" Colorcolumn and cursorline
set cursorline
set colorcolumn=80
hi CursorLine	ctermbg=black cterm=none
hi ColorColumn	ctermbg=black

" Popuo menu color changes
hi Pmenu	ctermbg=black guibg=black ctermfg=white guifg=white
hi PmenuSel	ctermbg=gray guibg=gray ctermfg=black guifg=black 
hi PmenuSbar	ctermbg=black guibg=black
hi PmenuThumb	ctermbg=white guibg=white

hi Folded	ctermbg=black

" Autoreloading the init.vim when changes are made
autocmd! bufwritepost init.vim source %

" Groff shit
autocmd! bufwritepost *.ms :silent !groff -e -ms "%" -Tpdf -Kutf8 > "%:r.pdf"

" Syntax highlighting
syntax enable
filetype plugin on

set rnu " Relative linenumbering
set nu  " Line numbers

set foldmethod=marker

" File searching
set path+=**
set wildmenu
set wildignore+=**/node_modules/** " Ignoring node_modules

" Ctags
command! MakeTags !ctags -R .

"{{{ Bindings

" Leader key
let mapleader = ","

" Indentation
vnoremap > >gv
vnoremap < <gv

"}}}
