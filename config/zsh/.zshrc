# aliases
source "$ZDOTDIR"/zsh_aliases

autoload -Uz compinit promptinit colors vcs_info

# version control
precmd_vcs_info() { vcs_info }
precmd_functions+=(precmd_vcs_info)

# git
zstyle ':vcs_info:git*' formats '%b:%r%m%u%c'
zstyle ':vcs_info:*' enable git

# prompt
setopt prompt_subst
colors
own_status_msg='%B%F{red}%(?..%? )%f%b'
own_hostname_msg='%B%F{green}%n%f@%F{blue}%m%f%b'
PROMPT='${own_status_msg}%~ %# '
RPROMPT='${vcs_info_msg_0_}'

# autocomplete
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit -d "$HOME"/.cache/zsh/zcompdump-"$ZSH_VERSION"
_comp_options+=(globdots)
setopt complete_aliases

# vi mode
bindkey -v

HISTFILE="$HOME"/.cache/zsh/history
HISTSIZE=25565
SAVEHIST=25565
setopt hist_ignore_space
setopt HIST_IGNORE_SPACE
setopt hist_ignore_dups
bindkey "^[[A" history-beginning-search-backward
bindkey "^[[B" history-beginning-search-forward

# Less colors
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

setopt auto_menu

# plugins
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh 2>/dev/null 
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
# make sure this is last
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
