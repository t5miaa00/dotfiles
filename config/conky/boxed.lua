-- Making some squares to handle the containers.
require 'helper'

function conky_main()
	if conky_window == nil then
		return
	end

	line_width = 1
	--width = conky_window.width - line_width
	height = 100
	pos_y = 1
	bg = {r=0, g=0, b=0, a=0.85}
	tx = {r=1, g=1, b=1, a=1}

	contents = {
		'${color3} Updates $color$hr'
	}

	DrawBox(pos_y, line_width, height, bg)
	DrawContent(contents[0], pos_y, height, tx)
end

--[[
${color3}Updates $color$hr
${font FreeMono:weight=bold:size=12}Pacman$font${goto 100}${execi 500 pacman -Qu | wc -l} \
${goto 150}│${font FreeMono:weight=bold:size=12}Aur$font${goto 250}${execi 500 yay --aur -Qu | wc -l}
$hr
${color3}File systems $color$hr
${font FreeMono:weight=bold:size=12}PATH${goto 100}USED/SIZE${alignr}USED%$font
/${goto 100}${fs_used /}/${fs_size /}$alignr${fs_used_perc /}%
/home${goto 100}${fs_used /home}/${fs_size /home}$alignr${fs_used_perc /home}%
$hr
${color3}Calendar $color$hr
${execpi 500 /home/aatu/.config/conky/cal.sh FreeMono}
${color3}Events $color$hr
${execpi 500 khal list today $(LC_ALL=C date +%A) -o -df "\${{font FreeMono:weight=bold:size=12}}{name} {date-long}\$font" }
$hr
]]
