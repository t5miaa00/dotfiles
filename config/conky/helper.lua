-- A helper file containing some classes to display things from
require 'cairo'

-- Create the cairo object
function CreateCairo()
	local cs = cairo_xlib_surface_create(
		conky_window.display,
		conky_window.drawable,
		conky_window.visual,
		conky_window.width,
		conky_window.height)
	local cr = cairo_create(cs)

	return cr, cs
end

-- Clean the cairo object
function CleanCairo(cr, cs)
	cairo_destroy(cr)
	cairo_surface_destroy(cs)
	cr = nil
end

function DrawBox(y_pos, line_width, height, color, line_alpha)
	cr, cs = CreateCairo()

	local _width = conky_window.width - line_width
	local _height = height - line_width
	local _x_pos = line_width / 2
	local _y_pos = y_pos + (line_width / 2)

	-- Draw the box
	cairo_set_line_width(cr, line_width)
	cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND)
	cairo_rectangle(cr, _x_pos, _y_pos, _width, _height)
	cairo_set_source_rgba(cr, color.r, color.g, color.b, color.a)
	cairo_fill_preserve(cr)
	cairo_set_source_rgba(cr, color.r, color.g, color.b, 1.0)
	cairo_stroke(cr)

	CleanCairo(cr, cs)
end

function DrawContent(cont, y_pos, height, color)
	cr, cs = CreateCairo()

	

	CleanCairo(cr, cs)
end
