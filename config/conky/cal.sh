#!/bin/sh

font="$1"

cal |sed -E '
1	s/(\w+ [[:digit:]]+)/${font '$font':weight=bold:size=12}${color2}\1${color}${font}/
2	s/('"$(date +%a)"')/${font '$font':weight=bold:size=12}${color2}\1${color}${font}/
3	s/( 1)/${color4}\1/
3,$	s/('"$(date +%_d)"')( )|('"$(date +%_d)"')$/${font Anonymous Pro:weight=bold:size=12}${color2}\1\3${color}${font}\2/
	s/^/${goto 72}/
'
