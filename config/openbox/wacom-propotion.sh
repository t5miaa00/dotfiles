#!/bin/bash
## Skripti, jolla saan tabletin toimimaan stna oikein
## käyttää xsetwacom sovellusta...

tester=`xsetwacom --list devices`
if [ -z "$tester" ]; then
    echo "No devices visible, exiting."
    exit 0;
fi

## Mitkä laitteet otetaan
## Kommentoimalla saa pois haluamattomia laitteita.
stylus=`xsetwacom --list devices |grep "type: STYLUS" |cut -f1`
eraser=`xsetwacom --list devices |grep "type: ERASER" | cut -f1`
cursor=`xsetwacom --list devices |grep "type: CURSOR" | cut -f1`
pad=`xsetwacom --list devices |grep "type: PAD" | cut -f1`
touch=`xsetwacom --list devices |grep "type: TOUCH" | cut -f1`

## Halutun ikkunan koordinaatit
screen="1920x1080+0+0"
#echo $stylus
#echo $eraser
#echo $cursor
#echo $pad
#echo $touch

## stylus
xsetwacom --set "$stylus" MapToOutput "$screen"
xsetwacom --set "$stylus" RawSample 10

## eraser
xsetwacom --set "$eraser" MapToOutput "$screen"
xsetwacom --set "$eraser" RawSample 10

## touch

## pad

## Tätä ei periaatteessa tarvita!
#notify-send "wacom-propotion.sh" "$stylus and \n$eraser set up properly!"
